package com.faceless.projectmanagement.serviceImpl;

import com.faceless.projectmanagement.model.Project;
import com.faceless.projectmanagement.repository.ProjectRepository;
import com.faceless.projectmanagement.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService {
    @Qualifier("projectRepository")
    @Autowired
    ProjectRepository projectRepository;
    
    @Override
    public void saveProject(Project project) {
        projectRepository.save(project);
        
    }

    @Override
    public void updateProject(Project project) {
        projectRepository.save(project);

    }

    @Override
    public Project findByProjectId(int projectId) {
        return projectRepository.findOne(projectId);
    }

    @Override
    public Project findByProjectName(String projectName) {
        return projectRepository.findByProjectName(projectName);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }
}
