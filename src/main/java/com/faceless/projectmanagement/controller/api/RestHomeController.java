//package com.faceless.projectmanagement.controller.api;
//
//
//import com.example.model.Employee;
//import com.example.service.EmployeeService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/api")
//public class RestHomeController {
//
//    @Autowired
//    EmployeeService employeeService;
//
//    @RequestMapping(value = "/employeeList" ,method = RequestMethod.GET)
//    public List<Employee> showEmployeeList(Model model){
//        PageRequest limit = new PageRequest(0,10);
//
////        model.addAttribute("employees",employeeService.findAll(limit).getContent());
////        model.addAttribute("successMessage", "User has been registered successfully");
////
////        return "employeeList";
//        return employeeService.findAll(limit).getContent();
//    }
//
//    @RequestMapping(value = "/employeeList/{from}" ,method = RequestMethod.POST)
//    public List<Employee> showEmployeeListOffset(@PathVariable("from")int from   , Model model){
//        PageRequest limit = new PageRequest(from,10);
//
////        model.addAttribute("employees",employeeService.findAll(limit).getContent());
////        model.addAttribute("successMessage", "User has been registered successfully");
////
////        return "employeeList";
//        return employeeService.findAll(limit).getContent();
//    }
//
//
//}
