package com.faceless.projectmanagement.model;


import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name ="employee_info")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "e_id")
    private int employeeId;

    @Column(name = "name",unique=true)
    @NotEmpty(message = "*Please provide your name")
    private String name;

    @Column(name = "working_address")
    @NotEmpty(message = "*Please provide your working address")
    private String workingAddress;

    @Lob
    @Column(name="photo")


    private byte[] photo;

    public Employee(String name, String workingAddress, byte[] photo) {
        this.name = name;
        this.workingAddress = workingAddress;
        this.photo = photo;
    }

    //    private int workMobile;
//
//    private int workLoacation;
//
//    private String workEmail;
//
//    private int workPhone;
//
//    private String department;
//
//    private String jobTitile;
//
//    private String manager;
//
//    private String coach;
//
//    //personal information
//
//    private String nationality;
//
//    private int idNo;
//
//    private int passportNo;
//
//    private int accountNo;
//
//    private String gender;
//
//    private boolean maritalStatus;
//
//    private Date dateOfBirth;

    public Employee() {
    }



    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkingAddress() {
        return workingAddress;
    }

    public void setWorkingAddress(String workingAddress) {
        this.workingAddress = workingAddress;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", name='" + name + '\'' +
                ", workingAddress='" + workingAddress + '\'' +
                '}';
    }
}
