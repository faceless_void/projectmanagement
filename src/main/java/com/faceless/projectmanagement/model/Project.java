package com.faceless.projectmanagement.model;


import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name ="project_info")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "projectId")
    private int projectId;

    @Column(name = "projectName",unique=true)
    private String projectName;
    @Column(name = "companyName")
    private String companyName;
    @Column(name = "startDate")
    private Date startDate;
    @Column(name = "endDate")
    private Date endDate;

//    @Column(name = "taskList",nullable = true)
//    private List<String> taskList;

    @Column(name = "latitude")
    private float latitude;
    @Column(name = "longitude")
    private float longitude;
    @Column(name = "lastUpdatedBy")
    private String lastUpdatedBy;
    @Lob
    @Column(name="photo")
    private byte[] photo;
    @Transient
    private List<Task> taskList;
    @Transient
    private List<String> taskNames;


    public Project(String projectName, String companyName, Date startDate, Date endDate, float latitude, float longitude, String lastUpdatedBy, byte[] photo, List<Task> taskList, List<String> taskNames) {
        this.projectName = projectName;
        this.companyName = companyName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lastUpdatedBy = lastUpdatedBy;
        this.photo = photo;
        this.taskList = taskList;
        this.taskNames = taskNames;
    }

    public Project() {
        taskList = new ArrayList<>();
        taskNames = new ArrayList<>();
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }



    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getLastUpdateBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdatedBy = lastUpdateBy;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public List<String> getTaskNames() {
        return taskNames;
    }

    public void setTaskNames(List<String> taskNames) {
        this.taskNames = taskNames;
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", taskList=" + taskList +
                ", taskNames=" + taskNames +
                '}';
    }
}
