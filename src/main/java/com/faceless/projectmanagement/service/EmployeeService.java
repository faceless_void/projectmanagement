//package com.faceless.projectmanagement.service;
//
//import com.example.model.Employee;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//
//import java.awt.print.Pageable;
//
//public interface EmployeeService {
//    void saveEmployee(Employee employee);
//
//    void updateUser(Employee employee);
//
//    Page<Employee> findAll(PageRequest limit);
//
//    Employee findById(int id);
//
//    Employee findByName(String name);
//
//    Page<Employee> findAll(Pageable limit);
//}
