package com.faceless.projectmanagement.repository;

import com.faceless.projectmanagement.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("projectRepository")
public interface ProjectRepository extends JpaRepository<Project,Integer> {
    Project findByProjectName(String projectName);
}
